"""Create object table

Revision ID: 690f20f8a74a
Revises: 
Create Date: 2021-07-03 19:30:45.894534

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = '690f20f8a74a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute(sa.text("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";"))

    op.create_table(
        'world',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(255), unique=True)
    )

    op.create_table(
        'object',
        sa.Column('id', UUID, primary_key=True, server_default=sa.text("uuid_generate_v4()")),
        sa.Column('world_id', sa.Integer, sa.ForeignKey('world.id'), nullable=False),
        sa.Column('citizen', sa.Integer, nullable=False),
        sa.Column('timestamp', sa.DateTime, nullable=False),
        sa.Column('x', sa.Integer, nullable=False),
        sa.Column('y', sa.Integer, nullable=False),
        sa.Column('z', sa.Integer, nullable=False),
        sa.Column('yaw', sa.Integer, nullable=False),
        sa.Column('tilt', sa.Integer, nullable=False),
        sa.Column('roll', sa.Integer, nullable=False),
        sa.Column('model', sa.String(255), nullable=False),
        sa.Column('description', sa.Text),
        sa.Column('action', sa.Text),
        sa.Column('line', sa.Text)
    )

def downgrade():
    op.drop_table('object')

    op.drop_table('world')

    op.execute(sa.text("DROP EXTENSION IF EXISTS \"uuid-ossp\";"))
