import math
import numpy
import pygltflib
import numpy as np

TAGS = {
    1: 'pelvis',
    2: 'back',
    3: 'neck',
    4: 'head',
    5: 'rtsternum',
    6: 'rtshoulder',
    7: 'rtelbow',
    8: 'rtwrist',
    9: 'rtfingers',
    10: 'lfsternum',
    11: 'lfshoulder',
    12: 'lfelbow',
    13: 'lfwrist',
    14: 'lffingers',
    15: 'rthip',
    16: 'rtknee',
    17: 'rtankle',
    18: 'rttoes',
    19: 'lfhip',
    20: 'lfknee',
    21: 'lfankle',
    22: 'lftoes',
    23: 'neck2',
    24: 'tail',
    25: 'tail2',
    26: 'tail3',
    27: 'tail4',
    28: 'obj1',
    29: 'obj2',
    30: 'obj3',
}


class RwxToGltf():
    """
    Converts parsed RWX models to GLTF
    """

    def __init__(self, rwx, texture_path=None):
        self.rwx = rwx

        self.texture_path = (lambda texture_name: texture_name + '.jpg') if texture_path is None else texture_path

        self.animations = []
        self.buffer = b''
        self.bufferViews = []
        self.accessors = []
        self.materials = []
        self.material_dicts = []
        self.meshes = []
        self.nodes = []
        self.samplers = []
        self.textures = []
        self.images = []
        self.root_node = self.convert(self.rwx)

    def save(self, filename):
        gltf = pygltflib.GLTF2(
            scene=0,
            scenes=[pygltflib.Scene(nodes=[self.root_node])],
            nodes=self.nodes,
            animations=self.animations,
            materials=self.materials,
            meshes=self.meshes,
            buffers=[pygltflib.Buffer(byteLength=len(self.buffer))],
            bufferViews=self.bufferViews,
            accessors=self.accessors,
            samplers=self.samplers,
            images=self.images,
            textures=self.textures
        )
        gltf.set_binary_blob(self.buffer)
        gltf.convert_buffers(pygltflib.BufferFormat.DATAURI)
        gltf.save_json(filename)

    def add_to_buffer(self, data):
        offset = len(self.buffer)

        length = len(data)

        self.buffer += data

        # Make sure we stay aligned to 4 bytes
        if(length % 4 != 0):
            self.buffer += b'0' * (4 - (length % 4))

        return (offset, length)

    def add_to_buffer_view(self, buffer_view):
        result = len(self.bufferViews)
        self.bufferViews.append(buffer_view)
        return result

    def add_to_accessor(self, accessor):
        result = len(self.accessors)
        self.accessors.append(accessor)
        return result

    def add_to_animations(self, animation):
        result = len(self.animations)
        self.animations.append(animation)
        return result

    def add_texture(self, texture_name):
        if(len(self.samplers) == 0):
            self.samplers.append(pygltflib.Sampler())

        image = -1
        for i, img in enumerate(self.images):
            if(img.uri == texture_name):
                image = i
        if image == -1:  # Not found
            image = len(self.images)
            self.images.append(pygltflib.Image(uri=self.texture_path(texture_name)))

        for i, tex in enumerate(self.textures):
            if(tex.source == image):
                return i

        result = len(self.textures)
        self.textures.append(pygltflib.Texture(sampler=0, source=image))

        return result

    def add_to_material(self, materials):
        material = {
            "r": 0.5,
            "g": 0.5,
            "b": 0.5,
            "a": 1.0,
            "metallic": 0.5,
            "roughness": 0.7
        }
        for m in materials:
            if(m['type'] == 'color'):
                material['r'] = m['r']
                material['g'] = m['g']
                material['b'] = m['b']
            elif(m['type'] == "opacity"):
                material['a'] = m['opacity']
            elif(m['type'] == "texture"):
                material['texture'] = m['texture']

        # See if material exists already
        for (i, m) in enumerate(self.material_dicts):
            if material == m:
                return i

        # Create new one
        result = len(self.materials)
        self.material_dicts.append(material)

        if('texture' in material and material['texture']):
            texture = self.add_texture(material['texture'])

        self.materials.append(pygltflib.Material(
            pbrMetallicRoughness={
                "baseColorFactor": [material['r'], material['g'], material['b'], material['a']],
                "baseColorTexture": {
                    "index": texture
                } if 'texture' in material and material['texture'] else None,
                "metallicFactor": material['metallic'],
                "roughnessFactor": material['roughness'],
            },
            alphaCutoff=None
        ))
        return result

    def convert(self, rwx, transform=None):
        node_index = len(self.nodes)
        node = pygltflib.Node(
            name=TAGS[rwx['tag']] if rwx['tag'] in TAGS else None
        )
        if transform is not None and not numpy.array_equal(transform, numpy.identity(4)):
            node.matrix = transform.flatten().tolist()
        self.nodes.append(node)

        matrix_stack = [numpy.identity(4), ]
        if 'transforms' in rwx and len(rwx['transforms']) > 0:
            for transform in rwx['transforms']:
                if(transform['type'] == "transform"):
                    matrix_stack.append(
                        numpy.array(transform['matrix']).reshape(4, 4)
                    )
                elif(transform['type'] == "identity"):
                    matrix_stack.append(matrix_stack[0])
                elif(transform['type'] == "scale"):
                    matrix = numpy.identity(4)
                    matrix[0, 0] = transform['x']
                    matrix[1, 1] = transform['y']
                    matrix[2, 2] = transform['z']

                    matrix_stack.append(numpy.dot(matrix, matrix_stack[-1]))
                elif(transform['type'] == "translate"):
                    matrix = numpy.identity(4)
                    matrix[3, :3] = [transform['x'],
                                     transform['y'],
                                     transform['z']]

                    matrix_stack.append(numpy.dot(matrix, matrix_stack[-1]))
                elif(transform['type'] == "rotate"):
                    x, y, z, rad = (transform['x'], transform['y'], transform['z'],
                                    math.radians(transform['angle']))
                    length = 1 / math.sqrt(x*x + y*y + z*z)
                    x = x * length
                    y = y * length
                    z = z * length

                    s = math.sin(rad)
                    c = math.cos(rad)
                    t = 1 - c

                    matrix = numpy.array([[x * x * t + c,
                                           y * x * t + z * s,
                                           z * x * t - y * s,
                                           0.0],
                                          [x * y * t - z * s,
                                           y * y * t + c,
                                           z * y * t + x * s,
                                           0.0],
                                          [x * z * t + y * s,
                                           y * z * t - x * s,
                                           z * z * t + c,
                                           0.0],
                                          [0.0, 0.0, 0.0, 1.0]])

                    matrix_stack.append(numpy.dot(matrix, matrix_stack[-1]))

        if 'vertices' in rwx and 'triangles' in rwx and len(rwx['vertices']) > 0 and len(['triangles']) > 0:
            uvs_present = any(['u' in v for v in rwx['vertices']])
            vertex_data = []
            for v in rwx['vertices']:
                vec = np.array([v['x'], v['y'], v['z'], 1.0])
                vec_transformed = vec.dot(matrix_stack[v['transform']])

                if uvs_present:
                    try:
                        vertex_data.append(
                            np.append(vec_transformed[:-1], [v['u'], v['v']]))
                    except KeyError:
                        vertex_data.append(
                            np.append(vec_transformed[:-1], [0.0, 0.0]))
                else:
                    vertex_data.append(vec_transformed[:-1])

            vertex_data = np.array(vertex_data, dtype="float32")

            points_accessor = len(self.accessors)
            self.accessors.append(pygltflib.Accessor(
                bufferView=len(self.bufferViews),
                componentType=pygltflib.FLOAT,
                count=len(vertex_data),
                type=pygltflib.VEC3,
                max=vertex_data.max(axis=0).tolist()[:3],
                min=vertex_data.min(axis=0).tolist()[:3]
            ))

            (vertex_offset, vertex_length) = self.add_to_buffer(
                vertex_data.flatten().tobytes())
            self.bufferViews.append(pygltflib.BufferView(
                buffer=0,
                byteOffset=vertex_offset,
                byteLength=vertex_length,
                byteStride=20 if uvs_present else 12,
                target=pygltflib.ARRAY_BUFFER
            ))

            if uvs_present:
                uvs_accessor = len(self.accessors)
                self.accessors.append(pygltflib.Accessor(
                    bufferView=len(self.bufferViews),
                    componentType=pygltflib.FLOAT,
                    count=len(vertex_data),
                    type=pygltflib.VEC2,
                    max=vertex_data.max(axis=0).tolist()[3:5],
                    min=vertex_data.min(axis=0).tolist()[3:5]
                ))

                self.bufferViews.append(pygltflib.BufferView(
                    buffer=0,
                    byteOffset=vertex_offset + 12,
                    byteLength=vertex_length,
                    byteStride=20,
                    target=pygltflib.ARRAY_BUFFER
                ))

            grouped_triangles = {}
            for triangle in rwx['triangles']:
                if triangle['material'] in grouped_triangles:
                    grouped_triangles[triangle['material']].append(
                        triangle['indices'])
                else:
                    grouped_triangles[triangle['material']] = [
                        triangle['indices']]

            primitives = []
            for (material, triangles) in grouped_triangles.items():
                short_indices = np.array(triangles).flatten().max() < 255
                index_data = np.array(
                    triangles, dtype="uint8" if short_indices else "uint16")-1

                index_accessor = len(self.accessors)
                self.accessors.append(pygltflib.Accessor(
                    bufferView=len(self.bufferViews),
                    componentType=pygltflib.UNSIGNED_BYTE if short_indices else pygltflib.UNSIGNED_SHORT,
                    count=len(index_data.flatten()),
                    type=pygltflib.SCALAR,
                    min=[int(index_data.min())],
                    max=[int(index_data.max())]
                ))

                (index_offset, index_length) = self.add_to_buffer(
                    index_data.flatten().tobytes())
                self.bufferViews.append(pygltflib.BufferView(
                    buffer=0,
                    byteOffset=index_offset,
                    byteLength=index_length,
                    target=pygltflib.ELEMENT_ARRAY_BUFFER
                ))

                material = self.add_to_material(rwx['materials'][0:material])

                primitives.append(pygltflib.Primitive(
                    attributes=pygltflib.Attributes(
                        POSITION=points_accessor,
                        TEXCOORD_0=uvs_accessor if uvs_present else None
                    ),
                    indices=index_accessor,
                    material=material
                ))

            if len(primitives) > 0:
                mesh = pygltflib.Mesh(primitives=primitives)

                mesh_index = len(self.meshes)
                self.meshes.append(mesh)

                node.mesh = mesh_index

        if 'children' in rwx and len(rwx['children']) > 0:
            for child in rwx['children']:
                child_matrix = matrix_stack[child['transform']]

                node.children.append(
                    self.convert(child['clump'],
                                 transform=child_matrix
                                 ))

        return node_index

    def add_anim(self, seq):
        print('Adding anim', seq)
