import os, glob
import zipfile

from rwxreader import RwxReader
from rwxtogltf import RwxToGltf

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description='Converts RWX files to GLTF'
    )
    parser.add_argument('model', type=str, nargs=1)
    parser.add_argument('--output', type=str, default=None)

    args = parser.parse_args()

    model_path = args.model[0]
    with open(model_path, encoding='iso-8859-1') as f:
        rwx = RwxReader(f)
    gltf = RwxToGltf(rwx.model)

    if args.output is None:
        out_path = os.path.splitext(model_path)[0] + '.gltf'
    else:
        out_path = args.output

    gltf.save(out_path)
