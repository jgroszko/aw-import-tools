"""Create coordinate index

Revision ID: d78023063ffc
Revises: 690f20f8a74a
Create Date: 2021-07-03 19:44:06.552913

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd78023063ffc'
down_revision = '690f20f8a74a'
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(
        'ik_object_coords',
        'object',
        [
            'world_id', 'x', 'z'
        ]
    )

def downgrade():
    op.drop_index('ik_object_coords')
