if __name__ == "__main__":
    import argparse
    import json
    import os, io
    import os.path
    import zipfile
    import gzip
    import logging

    FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger(__name__)

    from rwxreader import RwxReader
    from rwxtogltf import RwxToGltf

    parser = argparse.ArgumentParser(description="Converts models and avatars")
    parser.add_argument("config", type=str, nargs=1)

    args = parser.parse_args()

    with open(args.config[0]) as f:
        config = json.load(f)

    config_path = os.path.dirname(args.config[0])

    if not os.path.exists(os.path.join(config_path, config['output'])):
      os.mkdir(os.path.join(config_path, config['output']))

    models_output = os.path.join(config_path, config['output'], 'models')
    if not os.path.exists(models_output):
      os.mkdir(models_output)

    textures_path = lambda texture_path: os.path.join(config['textures_path'], texture_path + '.jpg')
    models_path = os.path.join(config_path, config['models_dir'])
    zip_password = config['zip_password'].encode('ascii')
    bad_files = []
    for file in [
        f for f in os.listdir(models_path)
        if (
          os.path.isfile(os.path.join(models_path, f)) and
          f.endswith('.zip')
        )
    ]:
        try:
          with zipfile.ZipFile(os.path.join(models_path, file), 'r') as zip:
            with io.TextIOWrapper(zip.open(zip.namelist()[0], mode='r', pwd=zip_password), encoding='iso-8859-1') as f:
              rwx = RwxReader(f)
        except zipfile.BadZipFile:
          try:
            with gzip.open(os.path.join(models_path, file), mode='rt', encoding='iso-8859-1') as f:
              rwx = RwxReader(f)
          except OSError:
            logger.warn("%s Couldn't unzip", file)
            bad_files.append(file)
        except RuntimeError:
          logger.warn("%s Bad password", file)
          bad_files.append(file)
        except Exception as e:
          logger.warn("%s Other exception %s %s", file, type(e), e)
          bad_files.append(file)

        try:
          gltf = RwxToGltf(rwx.model, texture_path=textures_path)
          gltf.save(
            os.path.join(models_output, os.path.splitext(file)[0] + '.gltf')
          )
        except Exception as e:
          logger.error("%s bad conversion %s %s", file, type(e), e)
          bad_files.append(file)
    
    logger.warn("Couldn't open %s files", len(bad_files))
