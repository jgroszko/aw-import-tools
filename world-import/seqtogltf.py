import numpy
import pygltflib
FRAMES_PER_SECOND = 30

def add_anim(gltf, seq, name=None):
  samplers = []
  channels = []

  for joint in seq.joints:
    node_ids = [i for (i, n) in enumerate(gltf.nodes) if n.name == joint]

    if len(node_ids) > 0:
      input_data = numpy.array(
        [f['frame'] / FRAMES_PER_SECOND for f in seq.joints[joint]['frames']],
        dtype="float32"
      )
      output_data = numpy.array(
        [[
          -f['quat'][1], -f['quat'][2], f['quat'][3], f['quat'][0]
        ] for f in seq.joints[joint]['frames']], 
        dtype="float32"
      ) 

      (input_offset, input_length) = gltf.add_to_buffer(input_data.flatten().tobytes())
      (output_offset, output_length) = gltf.add_to_buffer(output_data.flatten().tobytes())

      input_view = gltf.add_to_buffer_view(pygltflib.BufferView(
        buffer=0,
        byteOffset=input_offset,
        byteLength=input_length
      ))

      input_accessor = gltf.add_to_accessor(pygltflib.Accessor(
        bufferView=input_view,
        componentType=pygltflib.FLOAT,
        count=len(input_data),
        type=pygltflib.SCALAR,
        max=[input_data.max(axis=0).tolist()],
        min=[input_data.min(axis=0).tolist()]
      ))

      output_view = gltf.add_to_buffer_view(pygltflib.BufferView(
        buffer=0,
        byteOffset=output_offset,
        byteLength=output_length
      ))

      output_accessor = gltf.add_to_accessor(pygltflib.Accessor(
        bufferView=output_view,
        componentType=pygltflib.FLOAT,
        count=len(output_data),
        type=pygltflib.VEC4,
        max=output_data.max(axis=0).tolist(),
        min=output_data.min(axis=0).tolist()
      ))

      channels.append(pygltflib.AnimationChannel(
        sampler=len(samplers),
        target=pygltflib.AnimationChannelTarget(
          node=node_ids[0],
          path=pygltflib.ROTATION
        )
      ))

      samplers.append(pygltflib.AnimationSampler(
        input=input_accessor,
        output=output_accessor,
        interpolation=pygltflib.ANIM_LINEAR
      ))

  gltf.add_to_animations(pygltflib.Animation(
    name=name,
    samplers=samplers,
    channels=channels
  ))