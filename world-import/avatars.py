import os.path

from rwxreader import RwxReader
from seqreader import SeqReader
from rwxtogltf import RwxToGltf
from seqtogltf import add_anim

if __name__=="__main__":
  import argparse

  parser = argparse.ArgumentParser(
    description='Converts RWX files to GLTF and adds animations from SEQs'
  )
  parser.add_argument('avatar', type=str, nargs=1)
  parser.add_argument('seqs', type=str, nargs='+')
  parser.add_argument('--output', type=str, default=None)

  args = parser.parse_args()
  
  model_path = args.avatar[0]
  with open(model_path) as f:
    rwx = RwxReader(f)
  gltf = RwxToGltf(rwx.model)

  for seq_path in args.seqs:
    seq = SeqReader(seq_path)
    add_anim(gltf, seq, os.path.splitext(seq_path)[0])

  if args.output is None:
    out_path = os.path.splitext(model_path)[0] + '.gltf'
  else:
    out_path = args.output

  gltf.save(out_path)
