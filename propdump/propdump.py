import datetime
from timeit import default_timer as timer
from sqlalchemy import insert, create_engine, MetaData, Table

LINE_INCREMENT = 100000

def readuntil(f, char):
  result = ''
  while True:
    next = f.read(1)
    if(next == char or next == ''):
      break
    result += next
  return result

def object_generator(stream):
  line_no = 0
  while True:
    try:
      token = readuntil(stream, ' ')
      if(token == ''):
        break
      elif(token == "propdump"):
        version = readuntil(stream, '\n')
        assert(version == "version 4")
      else:
        citizen = int(token)
        timestamp = datetime.datetime.fromtimestamp(int(readuntil(stream, ' ')))
        coords = (
          int(readuntil(stream, ' ')),
          int(readuntil(stream, ' ')),
          int(readuntil(stream, ' '))
        )
        orientation = (
          int(readuntil(stream, ' ')),
          int(readuntil(stream, ' ')),
          int(readuntil(stream, ' '))
        )

        extra = int(readuntil(stream, ' '))

        model_len = int(readuntil(stream, ' '))
        desc_len = int(readuntil(stream, ' '))
        action_len = int(readuntil(stream, ' '))

        extra_2 = int(readuntil(stream, ' '))

        model = stream.read(model_len)
        description = stream.read(desc_len)
        action = stream.read(action_len)

        if(model.endswith('.rwx')):
          yield {
            'citizen': citizen,
            'timestamp': timestamp,
            'coords': coords,
            'orientation': orientation,
            'extra': extra,
            'extra_2': extra_2,
            'model': model,
            'description': description,
            'action': action
          }

        remaining = readuntil(stream, '\n')
        if(len(remaining) > 0 and extra == 0):
          print("WARNING - Line {} - Had some characters left over: {}".format(line_no, remaining))
    except Exception as e:
      print("Problem on line {} {}".format(line_no, e))

    line_no += 1

if __name__=="__main__":
  import argparse

  parser = argparse.ArgumentParser(description="Imports AW propdump files")
  parser.add_argument("world_id", type=int, nargs=1)
  parser.add_argument("propdump", type=str, nargs=1)

  args = parser.parse_args()

  engine = create_engine('postgresql://awtools:awtools@localhost/awtools')
  metadata = MetaData()
  object_table = Table("object", metadata, autoload_with=engine)

  with engine.connect() as conn:
    with open(args.propdump[0], encoding="windows-1252", mode="r") as f:
      last = timer()
      object_no = 0
      for object in object_generator(f):
        if object_no % LINE_INCREMENT == 0 and object_no > 0:
          new = timer()
          print("Objects {} to {} took {:.4f} seconds".format(object_no - LINE_INCREMENT, object_no, new - last))
          last = new

        stmt = insert(object_table).values(
          world_id=args.world_id[0],
          citizen=object['citizen'],
          timestamp=object['timestamp'],
          x=object['coords'][0],
          y=object['coords'][1],
          z=object['coords'][2],
          yaw=object['orientation'][0],
          tilt=object['orientation'][1],
          roll=object['orientation'][2],
          model=object['model'],
          description=object['description'],
          action=object['action']
        )

        result = conn.execute(stmt)

        object_no += 1

